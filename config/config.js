module.exports = {
  "database" : {
    "HOST": process.env.DB_HOST,
    "PORT": process.env.DB_PORT,
    "NAME": process.env.DB_NAME,
    "PASSWORD": process.env.DB_PASSWORD,
    "USER": process.env.DB_USER
  }
}