'use strict'

const _ = require('lodash');
const Base = require('../../helpers/base.controller');
const controller = new Base('register');
const { makeid , verify_and_upload_image_post, upload_images, dynamic_host } = require('../../helpers/utilities')
const validator = require('validator');


controller.postFunc = async function (req, res) {

    const {user_type,user,community,channel} = this.db
    const { name, last_name, username, type, organization, country, city, zip_code, address, country_code, phone, birthdate, email, password, gender, id_repository, id_role, id_community, nameCommunity, return_data} = req.body;
    let {codeCommunity} = req.body;
    const { comunity_code,invitation_code } = req.params;
    const jwt = require('jsonwebtoken');
    let profile_photo = null;

    const msg = [];
    try {

	const avatar = req.files ? req.files.avatar: null;
	profile_photo = verify_and_upload_image_post(avatar,"profile_photo");
	const archive = profile_photo ? profile_photo.split("_") : null;

	if (!!!name)
	    msg.push({ "name": "something went wrong, check name!" });

	if (!!!last_name)
	    msg.push({ "last_name": "something went wrong, check lastname!" });

	if (!!!username)
	    msg.push({ "username": "something went wrong, check username!" });

	if (!!!type)
	    msg.push({ "type": "something went wrong, check type!" });

	if (!!!organization)
	    msg.push({ "organization": "something went wrong, check organization!" });

	if (!!!country)
	    msg.push({ "country": "something went wrong, check country!" });





	if (validator.isEmpty(name))
	    msg.push({ "name": "It is empty" });

	if (validator.isEmpty(last_name))
	    msg.push({ "lastname": "It is empty" });

	if (validator.isEmpty(username))
	    msg.push({ "username": "It is empty" });

	if (validator.isEmpty(email))
	    msg.push({ "email": "It is empty" });

	if (!validator.isEmail(email) && !validator.isEmpty(email))
	    msg.push({ "email": "Format error" });

	if (validator.isEmpty(country))
	    msg.push({ "country": "It is empty" });

	if (validator.isEmpty(city))
	    msg.push({ "city": "It is empty" });

	if (validator.isEmpty(type))
	    msg.push({ "type": "It is empty" });

	if (validator.isEmpty(birthdate))
	    msg.push( { "birthdate": "It is empty" });

	if (validator.isEmpty(address))
	    msg.push( { "address": "It is empty" });

	if (validator.isEmpty(country_code))
	    msg.push( { "country_code": "It is empty" });

	if (validator.isEmpty(phone))
	    msg.push( { "phone": "It is empty" });

	if (validator.isEmpty(gender))
	    msg.push( { "gender": "It is empty" });

	if (  Object.keys(msg).length > 0 ) throw new Error(msg);


	let data = []
	let decoded
	if (invitation_code) {
	    decoded = jwt.verify(invitation_code, 'secret');
	    codeCommunity = comunity_code;

	    let query_invitation_code = await user_type.findOne({
		where: { invitation_code: decoded.data.invitation_code }
	    });

	    if (query_invitation_code) {
		throw new Error("Invitation code used!")
	    }
	}

	if (!nameCommunity && !codeCommunity) throw new Error("needs community")
	if (codeCommunity) {
	    data = await community.findOne({
		where: { code: codeCommunity },
		attributes: ['id', 'name']
	    });
	}


	if (!data) {
	    throw new Error("the code does not belong to any community!");
	}

	const host = dynamic_host(req);

	if(nameCommunity){
	    data = await community.create({
		name: nameCommunity,
		code: makeid(6)
	    }); 			

	    await channel.create({
		name: nameCommunity,
		description: nameCommunity,
		id_community: invitation_code ? decoded.data.community_id : data['id']
	    });
	}		

	let result = await user.create(
	    {
		name,
		last_name,
		username,
		type,
		zip_code,
		profile_photo,
		host,
		organization,
		country,
		city,
		address,
		country_code,    
		phone,
		birthdate,
		email,
		password,
		gender,
		id_repository,
	    });


	await user_type.create(
	    {
		id_user: result.id,
		id_role: id_role,
		id_community: invitation_code ? decoded.data.community_id : data['id'],
		invitation_code: invitation_code ? decoded.data.invitation_code : null
	    });

	if(profile_photo) upload_images( avatar, archive[0]+"_"+archive[1], archive[2].split(".")[0]);

	return this.response({
	    res,
	    statusCode: 201,
	    message: "Created Successfully",
	    payload: {
		result: return_data ? { id_community: data.id } : true
	    }
	});

    } catch (err) {
	return this.response({
	    res,
	    success: false,
	    statusCode: 500,
	    message: msg 
	});
    }
}

module.exports = controller;
